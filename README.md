### SpringBoot Web App for RESTfull API
This example includes
- Liquibase using H2 database (Example shows data loading)
- DevTools for hot reloads
- MapStruct for mapping between Entities and DTOs
- Lombok to minimize boilderplate code
- Custom JPA naming strategy to minimize Entity annotations
- Swagger-UI
- JUnit test to enforce DB fields
- Acutator to manage the application

To run the tests:
`mvn clean compile test` 

To run the Web App:
`mvn spring-boot:run`
