package com.example.demo.service;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.dto.GreetingDto;
import com.example.demo.mapper.GreetingMapper;
import com.example.demo.repository.GreetRepository;

@Transactional
@Service
public class GreetService {
	@Autowired
	GreetRepository aisGreetRepository;

	public List<GreetingDto> getAllGreetings(String name) {
		return StreamSupport.stream(aisGreetRepository.findAll().spliterator(), false).map(g -> {
			GreetingDto result = new GreetingDto();
			result.setId(g.getId());
			result.setVersion(g.getVersion());
			result.setGreeting(g.getGreeting());
			return result;
		}).collect(Collectors.toList());
	}

	public GreetingDto add(GreetingDto dto) {
		return GreetingMapper.INSTANCE.map(aisGreetRepository.save(GreetingMapper.INSTANCE.map(dto)));
	}

}
