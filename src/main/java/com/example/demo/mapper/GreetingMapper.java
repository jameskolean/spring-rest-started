package com.example.demo.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import com.example.demo.dto.GreetingDto;
import com.example.demo.entity.GreetEntity;

@Mapper
public interface GreetingMapper {

	GreetingMapper INSTANCE = Mappers.getMapper(GreetingMapper.class);

	GreetingDto map(GreetEntity entity);
	GreetEntity map(GreetingDto dto);
}
