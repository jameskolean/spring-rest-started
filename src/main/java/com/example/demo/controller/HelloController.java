package com.example.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.dto.GreetingDto;
import com.example.demo.service.GreetService;

@RestController
public class HelloController {
	@Autowired
	GreetService greetService;

	@RequestMapping("/v1/hello")
	public List<GreetingDto> sayHello() {
		return greetService.getAllGreetings("James");
	}

	@RequestMapping("/v1/add/{greeting}")
	public GreetingDto add(@PathVariable String greeting) {
		GreetingDto dto = new GreetingDto();
		dto.setGreeting(greeting);
		return greetService.add(dto);
	}
}
