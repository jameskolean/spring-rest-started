package com.example.demo.dto;

import java.util.UUID;

import lombok.Data;

@Data
public class GreetingDto {
	private UUID id;
	private Long version;
	private String greeting;
}
