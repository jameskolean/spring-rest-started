package com.example.demo.entity;

import static javax.persistence.TemporalType.TIMESTAMP;

import java.util.Date;
import java.util.UUID;

import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.Temporal;
import javax.persistence.Version;

import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import lombok.Data;

@Data
@MappedSuperclass
@EntityListeners(AuditingEntityListener.class)
abstract class Auditable<U> {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private UUID id;
	@Version
	private Long version;

	@CreatedBy
	U createdBy;

	@CreatedDate
	@Temporal(TIMESTAMP)
	Date createdDate;

	@LastModifiedBy
	U lastModifiedBy;

	@LastModifiedDate
	@Temporal(TIMESTAMP)
	Date lastModifiedDate;

}