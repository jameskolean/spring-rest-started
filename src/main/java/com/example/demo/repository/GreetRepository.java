package com.example.demo.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.example.demo.entity.GreetEntity;

@Repository
public interface GreetRepository extends CrudRepository<GreetEntity, String>, PagingAndSortingRepository<GreetEntity, String> {
}
