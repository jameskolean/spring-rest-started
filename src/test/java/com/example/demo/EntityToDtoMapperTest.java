package com.example.demo;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.UUID;

import org.junit.Test;

import com.example.demo.dto.GreetingDto;
import com.example.demo.entity.GreetEntity;
import com.example.demo.mapper.GreetingMapper;

public class EntityToDtoMapperTest {


	@Test
	public void givenEntity_whenMaps_thenCorrectDto() {
		// given
		UUID uuid = UUID.fromString("38400000-8cf0-11bd-b23e-10b96e4ef00d");
		Long version = 5L;
		String greeting = "Some Greeting";
		GreetEntity greetEntity = new GreetEntity();
		greetEntity.setId(uuid);
		greetEntity.setVersion(version);
		greetEntity.setGreeting(greeting);

		// when
		GreetingDto destination = GreetingMapper.INSTANCE.map(greetEntity);
		
		// then
		assertEquals(uuid, destination.getId());
		assertEquals(version, destination.getVersion());
		assertEquals(greeting, destination.getGreeting());
	}
	@Test
	public void givenDto_whenMaps_thenCorrectEntity() {
		// given
		UUID uuid = UUID.fromString("38400000-8cf0-11bd-b23e-10b96e4ef00d");
		Long version = 5L;
		String greeting = "Some Greeting";
		GreetingDto greetingDto = new GreetingDto();
		greetingDto.setId(uuid);
		greetingDto.setVersion(version);
		greetingDto.setGreeting(greeting);

		// when
		GreetEntity destination = GreetingMapper.INSTANCE.map(greetingDto);
		
		// then
		assertEquals(uuid, destination.getId());
		assertEquals(version, destination.getVersion());
		assertEquals(greeting, destination.getGreeting());
	}

}
