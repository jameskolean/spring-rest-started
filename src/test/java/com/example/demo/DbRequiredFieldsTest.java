package com.example.demo;

import static org.junit.Assert.assertTrue;

import java.lang.reflect.Field;
import java.util.Arrays;

import javax.persistence.Entity;

import org.apache.commons.lang3.ArrayUtils;
import org.junit.Test;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.ClassPathScanningCandidateComponentProvider;
import org.springframework.core.type.filter.AnnotationTypeFilter;

public class DbRequiredFieldsTest {

	@Test
	public void test() throws ClassNotFoundException {
		ClassPathScanningCandidateComponentProvider scanner = new ClassPathScanningCandidateComponentProvider(true);

		String[] requiredAttributes = { "id", "version", "createdBy", "createdDate", "lastModifiedBy",
				"lastModifiedDate" };
		scanner.addIncludeFilter(new AnnotationTypeFilter(Entity.class));

		for (BeanDefinition bd : scanner.findCandidateComponents("com.example.demo.entity")) {
			Class<?> forName = Class.forName(bd.getBeanClassName());
			Field[] fields = (Field[])ArrayUtils.addAll(forName.getDeclaredFields(), forName.getSuperclass().getDeclaredFields());
			for (String attribute : requiredAttributes) {
				boolean anyMatch = Arrays.stream(fields).anyMatch(f -> attribute.equals(f.getName()));
				assertTrue(bd.getBeanClassName() + " must have attribute " + attribute, anyMatch);
			}
		}
	}

}
